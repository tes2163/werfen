package com.systelab.kata;

import org.junit.Test;

import static com.systelab.kata.DirReduction.*;
import static org.junit.Assert.*;

public class DirReductionTest {
    @Test
    public void test1() {
        assertArrayEquals("\"NORTH\", \"SOUTH\", \"SOUTH\", \"EAST\", \"WEST\", \"NORTH\", \"WEST\"",
                new String[]{"WEST"},
                DirReduction.dirReduc(new String[]{"NORTH", "SOUTH", "SOUTH", "EAST", "WEST", "NORTH", "WEST"}));
    }

    @Test
    public void test2() {
        assertArrayEquals("\"NORTH\",\"SOUTH\",\"SOUTH\",\"EAST\",\"WEST\",\"NORTH\"",
                new String[]{},
                DirReduction.dirReduc(new String[]{"NORTH", "SOUTH", "SOUTH", "EAST", "WEST", "NORTH"}));
    }

    @Test
    public void test3() {
        try{
            DirReduction.dirReduc(null);
            fail("Test must fail");
        }catch (Exception e){
            assertEquals(IllegalArgumentException.class, e.getClass());
            assertEquals("Directions array cannot be null", e.getMessage());
        }
    }

    @Test
    public void test4() {
        assertArrayEquals("\"NORTH\",\"SOUTH\",\"SOUTH\",\"EAST\",\"WEST\",\"NORTH\"",
                new String[]{},
                DirReduction.dirReduc(new String[]{}));
    }

    @Test
    public void incompatibilities(){
        assertFalse(DirReduction.isIncompatible(NORTH, NORTH));
        assertTrue(DirReduction.isIncompatible(NORTH, SOUTH));
        assertFalse(DirReduction.isIncompatible(NORTH, EAST));
        assertFalse(DirReduction.isIncompatible(NORTH, WEST));
        assertTrue(DirReduction.isIncompatible(SOUTH, NORTH));
        assertFalse(DirReduction.isIncompatible(SOUTH, SOUTH));
        assertFalse(DirReduction.isIncompatible(SOUTH, EAST));
        assertFalse(DirReduction.isIncompatible(SOUTH, WEST));
        assertFalse(DirReduction.isIncompatible(EAST, NORTH));
        assertFalse(DirReduction.isIncompatible(EAST, SOUTH));
        assertFalse(DirReduction.isIncompatible(EAST, EAST));
        assertTrue(DirReduction.isIncompatible(EAST, WEST));
        assertFalse(DirReduction.isIncompatible(WEST, NORTH));
        assertFalse(DirReduction.isIncompatible(WEST, SOUTH));
        assertTrue(DirReduction.isIncompatible(WEST, EAST));
        assertFalse(DirReduction.isIncompatible(WEST, WEST));
    }
}

