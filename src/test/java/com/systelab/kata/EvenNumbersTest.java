package com.systelab.kata;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class EvenNumbersTest {
    @Test
    public void test1() {
        assertArrayEquals(new int[]{2, 4, 6}, EvenNumbers.divisibleBy(new int[]{1, 2, 3, 4, 5, 6}, 2));
    }

    @Test
    public void test2() {
        assertArrayEquals(new int[]{3, 6}, EvenNumbers.divisibleBy(new int[]{1, 2, 3, 4, 5, 6}, 3));
    }

    @Test
    public void test3() {
        assertArrayEquals(new int[]{0, 4}, EvenNumbers.divisibleBy(new int[]{0, 1, 2, 3, 4, 5, 6}, 4));
    }

    @Test
    public void dividerIsZero() {
        assertArrayEquals(new int[]{}, EvenNumbers.divisibleBy(new int[]{0, 1, 2}, 0));
    }

    @Test
    public void arrayIsEmpty() {
        assertArrayEquals(new int[]{}, EvenNumbers.divisibleBy(new int[]{}, 2));
    }

    @Test
    public void arrayIsNull() {
        try {
            EvenNumbers.divisibleBy(null, 2);
            fail("Test must fail");
        }catch (Exception e){
            assertEquals(IllegalArgumentException.class, e.getClass());
            assertEquals("Numbers array cannot be null", e.getMessage());
        }
    }
}
