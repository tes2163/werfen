package com.systelab.kata;

import java.util.ArrayList;
import java.util.List;

public class DirReduction {

    static final String NORTH = "NORTH";
    static final String SOUTH = "SOUTH";
    static final String EAST = "EAST";
    static final String WEST = "WEST";

    private static final List<String> incompatibilities = new ArrayList<>();

    static {
        incompatibilities.add(NORTH + SOUTH);
        incompatibilities.add(SOUTH + NORTH);
        incompatibilities.add(EAST + WEST);
        incompatibilities.add(WEST + EAST);
    }


    public static String[] dirReduc(String[] arr) {
        if (arr==null){
            throw new IllegalArgumentException("Directions array cannot be null");
        }
        List<String> list = new ArrayList<>();
        // Your code here.
        String last = null;
        for (String current : arr) {
            // If values are incompatible, last value will be removed
            if (isIncompatible(last, current)) {
                list.remove(last);
            }else{ // If values are compatible, current value will be added
                list.add(current);
            }
            last = current;
        }
        String[] retVal = list.toArray(new String[0]);
        // If size is different, any element was removed. Array must be validated again
        if (retVal.length != arr.length) {
            return dirReduc(retVal);
        }
        return retVal;
    }

    public static boolean isIncompatible(String dir1, String dir2) {
        if (dir1==null || dir2==null) {
            return false;
        }else {
            return incompatibilities.contains(dir1.toUpperCase() + dir2.toUpperCase());
        }
    }

}