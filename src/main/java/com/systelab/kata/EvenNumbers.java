package com.systelab.kata;

import java.util.stream.IntStream;

public class EvenNumbers {
    public static int[] divisibleBy(int[] numbers, int divider) {
        if (numbers==null){
            throw new IllegalArgumentException("Numbers array cannot be null");
        }
        // No exists any number divisible by zero
        if(divider==0){
            return new int[]{};
        }
        return IntStream.of(numbers).filter(i -> (i % divider) == 0).toArray();
    }
}
