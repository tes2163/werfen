package com.systelab.kata;

import java.util.ArrayList;
import java.util.List;

public class MexicanWave {
    public static String[] wave(String str) {
        if (str==null){
            throw new IllegalArgumentException("Wave array cannot be null");
        }
        List<String> retVal = new ArrayList<>();
        char[] chars = str.toCharArray();
        for (int i=0; i<chars.length; i++){
            if (chars[i]!=' '){
                String prefix = str.substring(0,i);
                String charToReplace = str.substring(i, i+1).toUpperCase();
                String suffix = str.substring(i+1);
                retVal.add(prefix+charToReplace+suffix);
            }
        }
        return retVal.toArray(new String[0]);
    }
}